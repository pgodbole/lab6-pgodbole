/**
 *  \file mm-cuda.cu
 *  \brief CUBLAS-based implementation of the local matrix multiply.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <cuda_runtime.h>
#include "cublas_v2.h"

static int isInit__ = 0; /*!< Set to '1' when handle is initialized, '-1' on error */
static cublasHandle_t handle__;

extern void sgemm_ (const char* opA, const char* opB,
		    const int* M, const int* N, const int* K,
		    const float* alpha, const float* A, const int* lda,
		    const float* B, const int* ldb,
		    const float* beta, float* C, const int* ldc);

static void mm_init (void);
static void assert_status (cublasStatus_t stat);

/* ======================================================================
 * Modify the three routines in this section (mm_local, mm_create, and
 * mm_free) to improve the efficiency of the GPU implementation.
 */

extern "C"
void
mm_local (const int m, const int n, const int k,
	  float* A_cpu_pinned, const int lda,
	  float* B_cpu_pinned, const int ldb,
	  float* C_cpu_pinned, const int ldc)
{
  mm_init ();
  assert (A_cpu_pinned || m <= 0 || k <= 0); assert (lda >= m);
  assert (B_cpu_pinned || k <= 0 || n <= 0); assert (ldb >= k);
  assert (C_cpu_pinned || m <= 0 || n <= 0); assert (ldc >= m);

  const float ONE = 1.0;
  cublasStatus_t stat = cublasSgemm (handle__, CUBLAS_OP_N, CUBLAS_OP_N,
				     m, n, k,
				     &ONE, A_cpu_pinned, lda, B_cpu_pinned, ldb,
				     &ONE, C_cpu_pinned, ldc);
  assert_status (stat);
  cudaDeviceSynchronize ();
  cudaFree (C_cpu_pinned);
  cudaFree (B_cpu_pinned);
  cudaFree (A_cpu_pinned);
}

extern "C"
float *
mm_create (int m, int n)
{
  const int num_bytes = m * n * sizeof (float);
  int alloc_flag = cudaHostAllocMapped | cudaHostAllocPortable;
  float* A_cpu_pinned;
  cudaHostAlloc ((void **)&A_cpu_pinned, num_bytes, alloc_flag);
  assert (A_cpu_pinned);
  return A_cpu_pinned;
}

extern "C"
void
mm_free (float* A)
{
  if (A) cudaFreeHost(A);
}

/* ====================================================================== */

static
void
mm_init (void)
{
  if (!isInit__) {
    cublasStatus_t stat = cublasCreate (&handle__);
    if (stat != CUBLAS_STATUS_SUCCESS) {
      fprintf (stderr, "*** CUBLAS initialization failure!\n");
      exit (-1);
    }
    isInit__ = 1;
  }
}

static
void
assert_status (cublasStatus_t stat)
{
  if (stat != CUBLAS_STATUS_SUCCESS) {
    switch (stat) {
    case CUBLAS_STATUS_NOT_INITIALIZED:
      fprintf (stderr, "CUBLAS_STATUS_NOT_INITIALIZED\n");
      break;
    case CUBLAS_STATUS_INVALID_VALUE:
      fprintf (stderr, "CUBLAS_STATUS_INVALID_VALUE\n");
      break;
    case CUBLAS_STATUS_ARCH_MISMATCH:
      fprintf (stderr, "CUBLAS_STATUS_ARCH_MISMATCH\n");
      break;
    case CUBLAS_STATUS_EXECUTION_FAILED:
      fprintf (stderr, "CUBLAS_STATUS_EXECUTION_FAILED\n");
      break;
    default:
      fprintf (stderr, "(unknown error)\n");
      break;
    }
  }
}

/* eof */
